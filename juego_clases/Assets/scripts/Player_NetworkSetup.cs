﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;


public class Player_NetworkSetup : NetworkBehaviour {

    [SerializeField] Camera CharacterCam;

	void Start () {
        if(!isLocalPlayer) return;

        GameObject.Find("Scene Camera").SetActive(false);
        CharacterCam.enabled = true;
	}

    void FixedUpdate(){

        if(!isLocalPlayer) return;
        //CharacterCam.transform.position = new Vector3(CharacterCam.transform.position.x, CharacterCam.transform.position.y, -4.0f);
    }
}
