﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Player_SyncRotation : NetworkBehaviour {
    
    [SyncVar] private Quaternion syncPlayerRotation;
    [SyncVar] private Quaternion syncCamRotation;

    [SerializeField] private Transform playerTransform;
    [SerializeField] private Transform camTransform;

    [SerializeField] private float lerpRate = 15;

	void FixedUpdate () {
        TransmitRotations();
        LerpRotations();
	
	}

    void LerpRotations(){
        if(isLocalPlayer) return;

        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, syncPlayerRotation, Time.deltaTime * lerpRate);
        camTransform.rotation = Quaternion.Lerp(camTransform.rotation, syncCamRotation, Time.deltaTime * lerpRate);
    }

    [Command]
    void CmdProvideRotationToServer(Quaternion playerRot, Quaternion camRot){
        syncPlayerRotation = playerRot;
        syncCamRotation = camRot;
    }

    [ClientCallback]
    void TransmitRotations(){
        if(!isLocalPlayer) return;
        CmdProvideRotationToServer(playerTransform.rotation, camTransform.rotation);
    }

}
